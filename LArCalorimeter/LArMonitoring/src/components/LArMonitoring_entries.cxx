#include "../LArCollisionTimeMonAlg.h"
#include "../LArAffectedRegionsAlg.h"
#include "../LArDigitMonAlg.h"
#include "../LArFEBMonAlg.h"
#include "../LArRODMonAlg.h"
#include "../LArHVCorrectionMonAlg.h"


DECLARE_COMPONENT(LArCollisionTimeMonAlg)
DECLARE_COMPONENT(LArAffectedRegionsAlg)
DECLARE_COMPONENT(LArDigitMonAlg)
DECLARE_COMPONENT(LArFEBMonAlg)
DECLARE_COMPONENT(LArRODMonAlg)
DECLARE_COMPONENT(LArHVCorrectionMonAlg)

